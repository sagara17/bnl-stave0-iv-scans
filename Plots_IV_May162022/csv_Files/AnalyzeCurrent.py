import numpy as np
import matplotlib.pyplot as plt

cmap = plt.cm.get_cmap('hsv',14)
for ModNo in np.arange(0,14):
    Channel=-1
    NMods = 0
    if (ModNo==0 or ModNo==1 or ModNo==2 or ModNo==3):
        Channel = 3
        NMods = 4
    elif (ModNo==4 or ModNo==5 or ModNo==6 or ModNo==7):
        Channel = 2
        NMods = 4
    elif (ModNo==8 or ModNo==9 or ModNo==10):
        Channel = 1
        NMods = 3
    elif (ModNo==11 or ModNo==12 or ModNo==13):
        Channel = 0
        NMods = 3
    MyIV = np.loadtxt('IV_c'+str(Channel)+'_m_'+str(ModNo)+'.csv',delimiter=',',skiprows=2).transpose()
    CorrI = (MyIV[2]-MyIV[1]/10)*1000 #Unit is nA now.
    #plt.errorbar(MyIV[1],CorrI,fmt='-ro')
    if(ModNo==0): plt.errorbar(MyIV[1], MyIV[1]/10, fmt='-b', label="Current through 10MOhm resistor")
    if(ModNo!=2): plt.errorbar(MyIV[1], MyIV[2], fmt='-o', c=cmap(ModNo), label="Module "+str(ModNo))
    plt.xlabel("ISEG Voltage [V]")
    plt.ylabel("ISEG Measured Current [uA]")
plt.title("IV for Modules on BNL Stave")
plt.legend(loc='best')
plt.show()

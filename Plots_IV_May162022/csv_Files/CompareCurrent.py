import numpy as np
import matplotlib.pyplot as plt

PreShipIV = np.loadtxt('IV_All_PreShipping.csv',delimiter=',',skiprows=3).transpose()

for ModNo in [2,3,9]:
    Channel= -1
    NCol = -1
    if (ModNo==2):
        Channel = 3
        NCol = 1
    elif (ModNo==3):
        Channel = 3
        NCol = 2
    elif (ModNo==9):
        Channel = 1
        NCol = 3
    MyIV = np.loadtxt('IV_c'+str(Channel)+'_m_'+str(ModNo)+'.csv',delimiter=',',skiprows=2).transpose()

    plt.errorbar(MyIV[0], MyIV[2], fmt='-bo', label="May 2022")
    plt.errorbar(-1*PreShipIV[0][:2*len(MyIV[0])], -1*PreShipIV[NCol][:2*len(MyIV[0])], fmt='-ro',  label="August 2020 (Compliance = -40 uA)")
    plt.errorbar(-1*PreShipIV[0][:2*len(MyIV[0])], -1*PreShipIV[NCol+3][:2*len(MyIV[0])], fmt='-ko',  label="October 2020 (Compliance = -40 uA)")
    plt.xlabel("ISEG Voltage [V]")
    plt.ylabel("ISEG Measured Current [uA]")
    plt.title("IV for Module " + str(ModNo) + " on BNL Stave")
    plt.legend(loc='best')
    #plt.show()
    plt.savefig('Comparison Module' +str(ModNo) +'.png')
    plt.savefig('Comparison Module' +str(ModNo) +'.pdf')
    plt.clf()

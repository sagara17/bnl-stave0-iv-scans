import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

pdf = PdfPages('IVPlots_Jul4.pdf')
for ModNo in np.arange(0,14):
    Channel=-1
    NMods = 0
    if (ModNo==0 or ModNo==1 or ModNo==2 or ModNo==3):
        Channel = 3
        NMods = 4
    elif (ModNo==4 or ModNo==5 or ModNo==6 or ModNo==7):
        Channel = 2
        NMods = 4
    elif (ModNo==8 or ModNo==9 or ModNo==10):
        Channel = 1
        NMods = 3
    elif (ModNo==11 or ModNo==12 or ModNo==13):
        Channel = 0
        NMods = 3

    plt.clf()
    MyIV0 = np.loadtxt('Plots_IV_Jul42022_FELIX/csv_Files/'+'IV_c'+str(Channel)+'_m_'+str(ModNo)+'.csv',delimiter=',',skiprows=2).transpose()
    plt.errorbar(MyIV0[1], MyIV0[2], fmt='-ro', label='FELIX')
    try:
        MyIV1 = np.loadtxt('Plots_IV_May162022/csv_Files/'+'IV_c'+str(Channel)+'_m_'+str(ModNo)+'.csv',delimiter=',',skiprows=2).transpose()
        plt.errorbar(MyIV1[1], MyIV1[2], fmt='-bo', label='ITSDAQ+G2')
    except:
        continue;
    if(ModNo//10 !=0):
        plt.errorbar(MyIV1[1], MyIV1[1]/10, fmt='--c', label='')
    #if(ModNo==2 or ModNo==3 or ModNo==9):
    #    MyIV2 = np.loadtxt('Module'+str(ModNo)+'_SensorIV.csv' ,delimiter=',',skiprows=2).transpose()
    #    MyIV3 = np.loadtxt('Module'+str(ModNo)+'_PreShipIV.csv',delimiter=',',skiprows=2).transpose()
    #    #plt.errorbar(-1*MyIV2[0],-1*MyIV2[1]-MyIV2[0]/10, fmt='-kv', label='Sensor IV')
    #    plt.errorbar(-1*MyIV3[0],-1*MyIV3[1], fmt='-g*', label='Pre Ship IV (Aug 2020)')
    #    plt.errorbar(-1*MyIV3[0],-1*MyIV3[2], fmt='-y*', label='Pre Ship IV (Oct 2020)')
    plt.xlabel("ISEG Voltage [V]")
    plt.ylabel("ISEG Measured Current [uA]")
    plt.title("IV for Module "+str(ModNo)+" on BNL Stave")
    plt.legend(loc='best')
    pdf.savefig()

pdf.close()

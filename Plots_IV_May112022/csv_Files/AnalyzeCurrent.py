import numpy as np
import matplotlib.pyplot as plt

for ModNo in np.arange(0,14):
    Channel=-1
    NMods = 0
    if (ModNo==0 or ModNo==1 or ModNo==2 or ModNo==3):
        Channel = 3
        NMods = 4
    elif (ModNo==4 or ModNo==5 or ModNo==6 or ModNo==7):
        Channel = 2
        NMods = 4
    elif (ModNo==8 or ModNo==9 or ModNo==10):
        Channel = 1
        NMods = 3
    elif (ModNo==11 or ModNo==12 or ModNo==13):
        Channel = 0
        NMods = 3
    MyIV = np.loadtxt('IV_c'+str(Channel)+'_m_'+str(ModNo)+'.csv',delimiter=',',skiprows=2).transpose()
    CorrI = (MyIV[2]*1000-MyIV[1]*100) #Unit is nA now.
    plt.errorbar(MyIV[1],MyIV[2],fmt='-ro')
    plt.errorbar(MyIV[1],MyIV[1]*NMods/10,fmt='-b')
    plt.title("IV for Module "+str(ModNo))
    plt.xlabel("ISEG Voltage [V]")
    plt.ylabel("ISEG Measured Current [uA]")
    plt.show()
